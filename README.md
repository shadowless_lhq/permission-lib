# PermissionLib

#### 软件架构

个人Android项目快速权限使用框架

#### 安装教程

Step 1. 添加maven仓库地址和配置

```
     //旧AndroidStudio版本
     //build.gradle
     allprojects {
         repositories {
            ...
              maven { url 'https://jitpack.io' }
         }
     }
     
     //新AndroidStudio版本
     //settings.gradle
     dependencyResolutionManagement {
          repositoriesMode.set(RepositoriesMode.FAIL_ON_PROJECT_REPOS)
          repositories {
            ...
             maven { url 'https://jitpack.io' }
          }
      }
```

Step 2. 添加依赖

a、克隆引入

直接下载源码引入model

b、远程仓库引入

[[![](https://jitpack.io/v/com.gitee.shadowless_lhq/permission-lib.svg)](https://jitpack.io/#com.gitee.shadowless_lhq/permission-lib)

```
     dependencies {
        implementation 'com.gitee.shadowless_lhq:permission-lib:Tag'
     }
```

#### 使用说明

```
     //需要获取特殊权限，需要继承CheckSpecialAdapter，实现特殊权限是否获取的逻辑
     public class CheckSpecialAdapter implements CheckSpecialCallBack {

          @RequiresApi(api = Build.VERSION_CODES.R)
          @Override
          public boolean checkSpecialPermission(Context context, String currentPermission) {
              //其中currentPermission为setSpecialPermission传入的权限组
              switch (currentPermission) {
                  case Settings.ACTION_MANAGE_APP_ALL_FILES_ACCESS_PERMISSION:
                      return Environment.isExternalStorageManager();
                  case Settings.ACTION_MANAGE_OVERLAY_PERMISSION:
                      return Settings.canDrawOverlays(context);
                  case Settings.ACTION_MANAGE_WRITE_SETTINGS:
                      return Settings.System.canWrite(context);
                  case Settings.ACTION_MANAGE_UNKNOWN_APP_SOURCES:
                      PackageManager manager = context.getPackageManager();
                      return manager.canRequestPackageInstalls();
                  default:
                      return false;
              }
          }
      }
```

```
     //初始化配置类
     PermissionConfig config = new PermissionConfig(FragmentActivity activity);
     PermissionConfig config = new PermissionConfig(Fragment fragment);
     //传入需要的普通权限
     config.setNormalPermission(String[] normalPermission);
     //传入需要的特殊权限和特殊权限适配器（需要获取特殊权限必传）
     config.setSpecialPermission(String[] specialPermission,CheckSpecialAdapter adapter);
     //请求权限（无回调）
     config.requestPermissions();
     //请求权限（有回调）
     config.requestPermissions(PermissionsFragment.PermissionCallBack callBack);
```
