package cn.com.shadowless.permissionlib;

import android.content.Context;

/**
 * The type Check special adapter.
 *
 * @author sHadowLess
 */
public abstract class CheckSpecialAdapter {

    /**
     * Check special permission boolean.
     *
     * @param context           the context
     * @param currentPermission the current permission
     * @return the boolean
     */
    public abstract boolean checkSpecialPermission(Context context, String currentPermission);
}
