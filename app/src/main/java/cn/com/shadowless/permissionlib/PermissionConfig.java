package cn.com.shadowless.permissionlib;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;

/**
 * The type Permissions.
 *
 * @author sHadowLess
 */
public class PermissionConfig {

    /**
     * The Tag.
     */
    private static final String TAG = PermissionConfig.class.getSimpleName();

    /**
     * The M permissions fragment.
     */
    private final Lazy<PermissionsFragment> mPermissionsFragment;

    /**
     * The Special permission.
     */
    private String[] specialPermission;
    /**
     * The Normal permission.
     */
    private String[] normalPermission;

    /**
     * The Adapter.
     */
    private CheckSpecialAdapter adapter;

    /**
     * Instantiates a new Permissions.
     *
     * @param activity the activity
     */
    public PermissionConfig(@NonNull final FragmentActivity activity) {
        mPermissionsFragment = getLazySingleton(activity.getSupportFragmentManager());
    }

    /**
     * Instantiates a new Permissions.
     *
     * @param fragment the fragment
     */
    public PermissionConfig(@NonNull final Fragment fragment) {
        mPermissionsFragment = getLazySingleton(fragment.getChildFragmentManager());
    }

    /**
     * Gets lazy singleton.
     *
     * @param fragmentManager the fragment manager
     * @return the lazy singleton
     */
    @NonNull
    private Lazy<PermissionsFragment> getLazySingleton(@NonNull final FragmentManager fragmentManager) {
        return new Lazy<PermissionsFragment>() {

            private PermissionsFragment permissionsFragment;

            @Override
            public synchronized PermissionsFragment get() {
                if (permissionsFragment == null) {
                    permissionsFragment = getPermissionsFragment(fragmentManager);
                }
                return permissionsFragment;
            }

        };
    }

    /**
     * Gets permissions fragment.
     *
     * @param fragmentManager the fragment manager
     * @return the permissions fragment
     */
    private PermissionsFragment getPermissionsFragment(@NonNull final FragmentManager fragmentManager) {
        PermissionsFragment permissionsFragment = findPermissionsFragment(fragmentManager);
        boolean isNewInstance = permissionsFragment == null;
        if (isNewInstance) {
            permissionsFragment = new PermissionsFragment();
            fragmentManager
                    .beginTransaction()
                    .add(permissionsFragment, TAG)
                    .commitNow();
        }
        return permissionsFragment;
    }

    /**
     * Find permissions fragment permissions fragment.
     *
     * @param fragmentManager the fragment manager
     * @return the permissions fragment
     */
    private PermissionsFragment findPermissionsFragment(@NonNull final FragmentManager fragmentManager) {
        return (PermissionsFragment) fragmentManager.findFragmentByTag(TAG);
    }

    /**
     * Sets special permission.
     *
     * @param specialPermission the special permission
     * @param adapter           the adapter
     */
    public void setSpecialPermission(@NonNull String[] specialPermission, @NonNull CheckSpecialAdapter adapter) {
        this.specialPermission = specialPermission;
        this.adapter = adapter;
    }

    /**
     * Sets normal permission.
     *
     * @param normalPermission the normal permission
     */
    public void setNormalPermission(@NonNull String[] normalPermission) {
        this.normalPermission = normalPermission;
    }

    /**
     * Request permissions.
     */
    public void requestPermissions() {
        requestPermissions(null);
    }

    /**
     * Request permissions.
     *
     * @param callBack the call back
     */
    public void requestPermissions(PermissionsFragment.PermissionCallBack callBack) {
        if (this.normalPermission == null && this.specialPermission == null) {
            throw new NullPointerException("至少传入一种类型的权限后再请求权限");
        }
        if (this.specialPermission != null && this.adapter == null) {
            throw new NullPointerException("特殊权限适配器必须和特殊权限一起传入");
        }
        if (this.normalPermission != null && this.specialPermission != null) {
            mPermissionsFragment.get().requestPermissions(this.normalPermission, this.specialPermission, this.adapter, callBack);
            return;
        }
        if (this.normalPermission != null) {
            this.specialPermission = new String[0];
            mPermissionsFragment.get().requestPermissions(this.normalPermission, this.specialPermission, this.adapter, callBack);
            return;
        }
        this.normalPermission = new String[0];
        mPermissionsFragment.get().requestPermissions(this.normalPermission, this.specialPermission, this.adapter, callBack);
    }

    /**
     * The interface Lazy.
     *
     * @param <V> the type parameter
     */
    @FunctionalInterface
    public interface Lazy<V> {
        /**
         * Get v.
         *
         * @return the v
         */
        V get();
    }

}
