package cn.com.shadowless.permissionlib;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;


/**
 * The type Permissions fragment.
 *
 * @author sHadowLess
 */
public final class PermissionsFragment extends Fragment {

    /**
     * The Activity.
     */
    private FragmentActivity activity;

    /**
     * The Special call back.
     */
    private CheckSpecialAdapter adapter;

    /**
     * The Call back.
     */
    private PermissionCallBack callBack;

    /**
     * The Current permission bean.
     */
    private SpecialInfo currentSpecialInfo;
    /**
     * The Special launcher.
     */
    private ActivityResultLauncher<Intent> specialLauncher = null;
    /**
     * The Normal launcher.
     */
    private ActivityResultLauncher<String[]> normalLauncherAfter23 = null;
    /**
     * The Normal launcher before 23.
     */
    private ActivityResultLauncher<String[]> normalLauncherBefore23 = null;

    /**
     * The List.
     */
    private final LinkedList<SpecialInfo> specialPermissionList = new LinkedList<>();
    /**
     * The Disagree permission.
     */
    private final List<String> disagreePermission = new ArrayList<>();
    /**
     * The Ban permission.
     */
    private final List<String> banPermission = new ArrayList<>();

    /**
     * The Normal permission.
     */
    private String[] normalPermission;

    /**
     * Instantiates a new Permissions fragment.
     */
    public PermissionsFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            initSpecialPermissionAfter30();
            initNormalPermissionAfter23();
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                initNormalPermissionAfter23();
            } else {
                initNormalPermissionBefore23();
            }
        }
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.activity = (FragmentActivity) context;
    }

    /**
     * Request permissions.
     *
     * @param normalPermissions  the normal permissions
     * @param specialPermissions the special permissions
     * @param adapter            the adapter
     * @param callBack           the call back
     */
    public void requestPermissions(@NonNull String[] normalPermissions, @NonNull String[] specialPermissions, @NonNull CheckSpecialAdapter adapter, PermissionCallBack callBack) {
        //其中一个不满足就可以请求权限
        if (normalPermissions.length == 0 && specialPermissions.length == 0) {
            return;
        }
        this.adapter = adapter;
        this.callBack = callBack;
        this.normalPermission = normalPermissions;
        specialPermissionList.clear();
        disagreePermission.clear();
        banPermission.clear();
        List<String> needRequestNormal = new ArrayList<>();
        for (String normal : normalPermissions) {
            if (!isGranted(normal)) {
                needRequestNormal.add(normal);
            }
        }
        String[] needNormalPermission = needRequestNormal.toArray(new String[0]);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            for (String permissionName : specialPermissions) {
                boolean hasPermission = this.adapter.checkSpecialPermission(activity, permissionName);
                if (hasPermission) {
                    continue;
                }
                SpecialInfo bean = new SpecialInfo();
                bean.setPermissionName(permissionName);
                bean.setIntent(getPermissionIntent(permissionName));
                specialPermissionList.add(bean);
            }
            if (needNormalPermission.length == 0 && specialPermissionList.isEmpty()) {
                if (this.callBack != null) {
                    this.callBack.agree();
                }
                return;
            }
            //若特殊权限不为空，先获取特殊权限，注册的合同逻辑再获取完特殊权限后
            //再去获取普通权限
            if (!specialPermissionList.isEmpty()) {
                launchSpecialPermission(specialPermissionList);
            } else {
                normalLauncherAfter23.launch(needNormalPermission);
            }
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                normalLauncherAfter23.launch(needNormalPermission);
            } else {
                normalLauncherBefore23.launch(needNormalPermission);
            }
        }
    }

    /**
     * Is granted boolean.
     *
     * @param permission the permission
     * @return the boolean
     */
    private boolean isGranted(String permission) {
        return ContextCompat.checkSelfPermission(activity, permission) == PackageManager.PERMISSION_GRANTED;
    }

    /**
     * Gets permission intent.
     *
     * @param permission the permission
     * @return the permission intent
     */
    private Intent getPermissionIntent(String permission) {
        Intent intent = new Intent(permission);
        Uri uri = Uri.fromParts("package", activity.getPackageName(), null);
        intent.setData(uri);
        return intent;
    }

    /**
     * Init special.
     */
    @RequiresApi(api = Build.VERSION_CODES.R)
    private void initSpecialPermissionAfter30() {
        if (specialLauncher == null) {
            specialLauncher = activity.registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), result -> {
                //检查当前获取的特殊权限是否获取成功
                //未成功，加入不同意列表
                if (!this.adapter.checkSpecialPermission(activity, this.currentSpecialInfo.getPermissionName())) {
                    disagreePermission.add(this.currentSpecialInfo.getPermissionName());
                }
                //若当前列表移除后大小为0，说明列表中的所有权限都走过了获取流程
                if (specialPermissionList.size() == 0) {
                    //若普通权限列表不为0
                    //则开始获取普通权限
                    if (normalPermission.length > 0) {
                        normalLauncherAfter23.launch(normalPermission);
                        return;
                    }
                    //若不同意列表为空，说明用户全部同意
                    if (disagreePermission.isEmpty()) {
                        if (this.callBack != null) {
                            this.callBack.agree();
                            return;
                        }
                    }
                    //否则返回不同意列表
                    if (this.callBack != null) {
                        this.callBack.disagree(disagreePermission);
                    }
                    return;
                }
                //若当前列表移除后大小不为0，则继续下一个权限的获取
                launchSpecialPermission(specialPermissionList);
            });
        }
    }

    /**
     * Gets permission.
     *
     * @param list the list
     */
    @RequiresApi(api = Build.VERSION_CODES.R)
    private void launchSpecialPermission(LinkedList<SpecialInfo> list) {
        this.currentSpecialInfo = list.poll();
        specialLauncher.launch(this.currentSpecialInfo.getIntent());
    }


    /**
     * Init normal permission after 23.
     */
    @RequiresApi(api = Build.VERSION_CODES.M)
    private void initNormalPermissionAfter23() {
        if (normalLauncherAfter23 == null) {
            normalLauncherAfter23 = activity.registerForActivityResult(new ActivityResultContracts.RequestMultiplePermissions(), result -> {
                for (String permission : result.keySet()) {
                    Boolean granted = result.get(permission);
                    boolean isRationale = activity.shouldShowRequestPermissionRationale(permission);
                    if (Boolean.FALSE.equals(granted) && !isRationale) {
                        banPermission.add(permission);
                    } else if (Boolean.FALSE.equals(granted) && isRationale) {
                        disagreePermission.add(permission);
                    }
                }
                if (banPermission.isEmpty() && disagreePermission.isEmpty()) {
                    if (callBack != null) {
                        callBack.agree();
                    }
                } else if (!banPermission.isEmpty()) {
                    if (callBack != null) {
                        callBack.ban(banPermission);
                    }
                } else {
                    if (callBack != null) {
                        callBack.disagree(disagreePermission);
                    }
                }
            });
        }
    }

    /**
     * Init normal permission before 23.
     */
    private void initNormalPermissionBefore23() {
        if (normalLauncherBefore23 == null) {
            normalLauncherBefore23 = activity.registerForActivityResult(new ActivityResultContracts.RequestMultiplePermissions(), result -> {
                for (String permission : result.keySet()) {
                    Boolean granted = result.get(permission);
                    if (Boolean.FALSE.equals(granted)) {
                        disagreePermission.add(permission);
                    }
                }
                if (disagreePermission.isEmpty()) {
                    if (callBack != null) {
                        callBack.agree();
                    }
                } else {
                    if (callBack != null) {
                        callBack.disagree(disagreePermission);
                    }
                }
            });
        }
    }

    /**
     * The interface Permission call back.
     */
    public interface PermissionCallBack {
        /**
         * Agree.
         */
        void agree();

        /**
         * Disagree.
         *
         * @param name the name
         */
        void disagree(List<String> name);

        /**
         * Ban.
         *
         * @param name the name
         */
        void ban(List<String> name);
    }
}
