package cn.com.shadowless.permissionlib;

import android.content.Intent;

/**
 * The type Permission bean.
 *
 * @author sHadowLess
 */
public class SpecialInfo {
    /**
     * The Permission name.
     */
    private String permissionName;
    /**
     * The Intent.
     */
    private Intent intent;

    /**
     * Gets permission name.
     *
     * @return the permission name
     */
    public String getPermissionName() {
        return permissionName == null ? "" : permissionName;
    }

    /**
     * Sets permission name.
     *
     * @param permissionName the permission name
     */
    public void setPermissionName(String permissionName) {
        this.permissionName = permissionName;
    }

    /**
     * Gets intent.
     *
     * @return the intent
     */
    public Intent getIntent() {
        return intent;
    }

    /**
     * Sets intent.
     *
     * @param intent the intent
     */
    public void setIntent(Intent intent) {
        this.intent = intent;
    }
}
